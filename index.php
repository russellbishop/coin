<?php $page = 'home'; ?>

<?php include('inc/header-top.php'); ?>

<?php include($INC.'header-bottom.php'); ?>

<?php 
	
	include($INC.'sections/bank-total.php');
	
	include($INC.'forms/form-send.php');
	
	include($INC.'sections/activity.php');
	
	include($INC.'popups/popup-budget-remaining.php');
	include($INC.'popups/popup-spent-this-month.php');

?>

<?php include($INC."footer-top.php"); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
<script src="<?=$JS;?>records-add.js"></script>
<script src="<?=$JS;?>activity-records-delete.js"></script>

<?php include($INC."footer-bottom.php"); ?>