<form class="form form-send container" action="<?=$ACTIONS?>records-add.php" method="post">
	
	<input name="send-amount" class="amount" type="number" placeholder="0" step="0.01" min="0"  />
	
    <select name="send-type" class="type" data-placeholder="What was it?" style="width: 100%;">
	    
	    
	    <option></option><?php /* this is for the placeholder text */ ?>
	    
	    <?php
		    
		foreach ($types as $type) {
			echo '<option value="' . $type['id'] . '">' . $type['type'] . '</option>';
		}
		
		?>
		
    </select>
    
    <input name="send-spend-earn" type="hidden" class="hidden-spend-earn" value="spend" />

	<div class="inset grid3">
		<button name="submit-spend" value="spend" class="submit-form submit submit-spend col" type="submit">Spend it</button>
		<button name="submit-earn" value="earn" class="submit-form submit submit-earn col" type="submit">Earn it</button>
	</div>
	
	<div class="loading-art"><img src="<?=$IMG;?>coin-large.svg" /></div>
	
</form>