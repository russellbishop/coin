<form class="form form-budget container" action="<?=$ACTIONS?>budgets-add.php" method="post">
	
	<fieldset>
		<label for="form-budget-type">What are you budgetting?</label>
	    <select name="payment_type" class="type" id="form-budget-type" data-placeholder="Choose&hellip;" style="width: 100%;">
		    
		    <?php /* this is for the placeholder text */ ?>
		    <option></option>
		    
		    <?php
			   
			if(!empty($_GET["set"])) {
				
				$setid = htmlspecialchars($_GET["set"]);
				
			}
			
			foreach ($types as $type) {
				
				if (empty($setid)) {
					
					echo '<option value="' . $type["id"] . '">' . $type["type"] . '</option>';
					
				} else if ($setid == $type["id"]) {
				
					echo '<option value="' . $type["id"] . '" selected="selected">' . $type["type"] . '</option>';
					
				} else {
					
					echo '<option value="' . $type["id"] . '">' . $type["type"] . '</option>';
					
				}
				
			}
			
			?>
			
	    </select>
	</fieldset>
    
    <fieldset>
	    <label for="form-budget-amount">Monthly budget</label>
	    <input type="number" name="amount" placeholder="0" step="0.01" min="0" class="amount" id="form-budget-amount" />
    </fieldset>

	<div class="inset">
		<button name="submitbudget" value="budget" class="submit-form submit submit-earn">New budget</button>
	</div>
	
	<div class="loading-art"><img src="<?=$IMG;?>coin-large.svg" /></div>
	
</form>