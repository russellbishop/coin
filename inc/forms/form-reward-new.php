<form action="<?=$ACTIONS?>reward-new.php" class="form-add-reward popup-body popup-base" method="post">
	<fieldset>
		<label for="form-rewards-description">What&rsquo;s the reward for?</label>
		<textarea name="form-rewards-description" id="form-rewards-description"></textarea>
	</fieldset>
	
	<fieldset>
		<label for="form-rewards-name">Give it a catchy name</label>
		<input type="text" name="form-rewards-name" id="form-rewards-name">
	</fieldset>
	
	<fieldset>
		<label for="form-rewards-value">How often could you do it?</label>
		
		<select type="number" name="form-rewards-value" id="form-rewards-value" placeholder="0" step="1" min="0" max="5" >
			<option value="1">Every day</option>
			<option value="2">Every few days</option>
			<option value="2">Once a week</option>
			<option value="2">Every couple of weeks</option>
			<option value="5">Once a month</option>
		</select>
	</fieldset>
	
	<button type="submit" class="submit submit-add">New Reward</button>
</form>