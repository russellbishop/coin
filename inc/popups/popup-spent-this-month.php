<script type="text/template" id="spent-this-month" />

    <div class="popup popup-generated popup-spent-this-month popup-open">
	    
	<span class="popup-delete popup-close-x">X</span>
	
		<div class="popup-body popup-base">
			
			<h4 class="type"><%= typename %></h4>
									
			<p>You&rsquo;ve spent <span class="amount credit credit-small minus"><%= spentthismonth * -1 %></span> on <%= typename %> this month.</p>
			
			<p>Would you like to set up a budget for <%= typename %>?</p>
			
			<p><a href="<?=$ROOT;?>budgets.php?set=<%= typeid %>" class="submit submit-spend">Yes please!</a></p>
			<p><a href="#" class="submit submit-add popup-delete">No thanks.</a></p>
			
		</div>
		
	</div>
    
</script>