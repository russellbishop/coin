<script type="text/template" id="budget-remaining" />

    <div class="popup popup-generated popup-budget-remaining popup-open">
	    
	<span class="popup-delete popup-close-x">X</span>
	
		<div class="popup-body popup-base">
									
			<% if (budgetremaining < 0) { %>
			
				<p class="budget-summary budget-overspent"><span class="amount credit credit-small minus"><%= budgetremaining * -1 %></span> overspent on <%= budgettype %>!</p><p><a href="#" class="submit submit-delete popup-delete">Shit!</a></p>
			
			<% } else { %>
				
				<p class="budget-summary budget-left">You have <span class="amount credit credit-small"><%= budgetremaining %></span> left to spend on <%= budgettype %>.</p><p><a href="#" class="submit submit-add popup-delete">Thanks!</a></p>
				
			<% } %>
			
		</div>
		
	</div>
    
</script>