<?php
	
	/*
		get every unique monthly field
	*/
	$monthlies = $database->select('monthly',
		[
			'[><]types-define' => ['type_id' => 'id']
		],
		
		[
			'monthly.amount',
			'types-define.id',
			'types-define.type'
		]
		
	);
	
?>