<?php






/*
	Get user data
*/

$account = $database->get("user", ["username", "paydate", "salary"]);






/*
	Get every type from types-define
*/

$types = $database->select('types-define',
	[
		'type', 'id'
	],
	
	[
		"ORDER" => ['type ASC']
	]
);






/*
	This month's array


$today = date("Y-m-d H:i:s");
$thismonth = date("Ym", strtotime($today));

// Before comma is the format, after is the date:
$dt = DateTime::createFromFormat('Ym d H:i:s', $thismonth . ' ' . $account["paydate"] . ' 00:00:00');
$et = clone $dt;

$et->modify('-1 month');

if (in_array($et->format('N'), array(6, 7))) {
	$et->modify('last weekday');
	}
if (in_array($dt->format('N'), array(6, 7))) {
	$dt->modify('last weekday');
}

$dt->modify('-1 second');

$thisMonthsDates = array($et->format('Y-m-d H:i:s'), $dt->format('Y-m-d H:i:s'));

*/







/*
$todaysdate = date('Y-m-d H:i:s');
	
$todaysmonth = date('m');

$todaysday = date('d');

$thismonthspaydate = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m') . '-' . $account["paydate"] . '00:00:00');

$previouspaydate = clone $thismonthspaydate;
$previouspaydate->modify('-1 month');

if ((new DateTime($todaysdate)) < $thismonthspaydate) {
	
	// today is before paydate;
	
	// if this is a weekend, modify to the previous friday
	if (in_array($thismonthspaydate->format('N'), array(6, 7))) {
		$thismonthspaydate->modify('last weekday');
	}
	
}
		
else {
	
	// today is after paydate;
	
	$thismonthspaydate->modify('+1 month');
	
	// if this is a weekend, modify to the previous friday
	if (in_array($thismonthspaydate->format('N'), array(6, 7))) {
		$thismonthspaydate->modify('last weekday');
	}	
	
}

if (in_array($previouspaydate->format('N'), array(6, 7))) {
	$previouspaydate->modify('last weekday');
}

$thisMonthsDates = array(
	$previouspaydate->format('Y-m-d H:i:s'),
	$thismonthspaydate->format('Y-m-d H:i:s')
);

*/




// create empty array
$thisMonthsDates = array();

$thisMonthStart = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m') . '-01 00:00:00');

$thisMonthEnd = clone $thisMonthStart;
$thisMonthEnd->modify('+1 month');
$thisMonthEnd->modify('-1 second');

$thisMonthsDates = array(
	$thisMonthStart->format('Y-m-d H:i:s'),
	$thisMonthEnd->format('Y-m-d H:i:s')
);
	




?>