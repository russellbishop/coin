<?php
require_once('../config/medoo.php');
include("../config/server.php");

	$id = $_POST['id'];

	$database->delete('records', [
		'id' => $id
	]);
	
	
	// Get new bank total
	$banktotal = round($database->sum('records', 'amount'));
			
	$return = array(
	    'newtotal' => $banktotal,
	    'id' => $id,
	);
	
	echo json_encode($return);
		
?>