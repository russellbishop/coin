<?php
require_once('../config/medoo.php');
include("../config/server.php");

$amount = $_POST['amount'];
$typeid = $_POST['payment_type'];
/*
	this is the type-id from types-define
	if the type is existing, it posts the type-define ID (e.g. 53)
	if the type is new, it posts the new name (e.g. Carrots)
*/

if (!empty($amount) && !empty($typeid)) {

	if ($database->has("types-define", [
		"OR" => [
			"type" => $typeid,
			"id" => $typeid
		]
	]))
	
	{
	
		// Exists, but maybe not in the same case sensitivity
			
		$database->insert('monthly', [
			'amount' => $amount,
			'type_id' => $typeid
		]);
		
		$return = array(
		    'typeid' => $typeid,
		);
		
		echo json_encode($return);
		
	}
	
	else {
		
		// New
		
		$newtypeid = $database->insert('types-define', [
			'type' => ucfirst($typeid),
		]);
		
		$database->insert('monthly', [
			'amount' => $amount,
			'type_id' => $newtypeid
		]);
		
		$return = array(
		    'typeid' => $newtypeid,
		);
		
		echo json_encode($return);
		
	}
	
}

else {
	
	die;
	
}
		
?>