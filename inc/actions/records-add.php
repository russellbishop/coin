<?php
require_once('../config/medoo.php');
include("../config/server.php");
include("../data/global.php");

	$paymenttype = $_POST['send-type'];
	/*
		this is the type-id from types-define
		if the type is existing, it posts the type-define ID (e.g. 53)
		if the type is new, it posts the new name (e.g. Carrots)
	*/
	
	$hiddeninput = $_POST['send-spend-earn'];
	$amount = $_POST['send-amount'];
	$datetime = date("Y-m-d H:i:s");
	
	if (empty($paymenttype) || empty($amount))  {
		
		die;
			
	}
	
	else {
	
		if ($database->has("types-define",
		
			// Do we already have this type defined?
			
			[
				"OR" => [
					"type" => $paymenttype,
					"id" => $paymenttype
				]
			]
		))
		
		{
			// Exists, but maybe not in the same case sensitivity
			
			if ( $hiddeninput == 'spend' ) {
				
				$database->insert('records', [
					'amount' => '-' . $amount,
					'type_id' => $paymenttype,
					'datetime' => $datetime
				]);
				
			}
			
			elseif ( $hiddeninput == 'earn' ) {
				
				$database->insert('records', [
					'amount' => $amount,
					'type_id' => $paymenttype,
					'datetime' => $datetime
				]);
				
			}
			
			// find a budget
			$budgetamount = $database->get('budgets', ['id', 'amount'], [
				'type_id' => $paymenttype
			]);
			
			// Get name of type
			$counttitle = $database->get('types-define', ['type', 'id'], [
				'id' => $paymenttype
			]);
			
			// Total spent from budget
			$budgetsum = $database->sum('records', 'amount', [
				'AND' => [
					'datetime[<>]' => $thisMonthsDates,
					'type_id' => $paymenttype
				]
			]);
			
			$budgetremaining = ($budgetamount["amount"] - ($budgetsum * -1));
			
			// get the new bank total
			$banktotal = round($database->sum('records', 'amount'));
			
			if ($budgetamount > 0) {
				
				$isbudget = 'yes';
				
				$return = array(
				    'newtotal' => $banktotal,
				    'isbudget' => $isbudget,
				    'budgettype' => $counttitle["type"],
				    'budgetremaining' => $budgetremaining,
				);
				
			} else {
				
				$isbudget = 'no';
				
				// Total spent of this type
				$spentthismonth = $database->sum('records', 'amount', [
					'AND' => [
						'datetime[<>]' => $thisMonthsDates,
						'type_id' => $paymenttype
					]
				]);
				
				$return = array(
				    'newtotal' => $banktotal,
				    'isbudget' => $isbudget,
				    'typename' => $counttitle["type"],
				    'typeid' => $paymenttype,
				    'spentthismonth' => $spentthismonth,
				);
				
			}
			
			
			
			
			
			echo json_encode($return);
				
		}
		
		else {
			
			// New
			
			$newtypeid = $database->insert('types-define', [
				'type' => ucfirst($paymenttype),
			]);
			
			if ( $hiddeninput == 'spend' ) {
				
				$database->insert('records', [
					'amount' => '-' . $amount,
					'type_id' => $newtypeid,
					'datetime' => $datetime
				]);
				
			}
			
			elseif ( $hiddeninput == 'earn' ) {
				
				$database->insert('records', [
					'amount' => $amount,
					'type_id' => $newtypeid,
					'datetime' => $datetime
				]);
				
			}
			
			$banktotal = round($database->sum('records', 'amount'));
			
			$return = array(
			    'typeis' => 'new',
			    'typeid' => $newtypeid,
			    'newtotal' => $banktotal,
			);
			
			echo json_encode($return);
			
		}
		
	} // end validation else
	
?>