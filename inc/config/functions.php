<?php
	
	function timeSince ($time) {
	
	    $time = date(time() + 1) - $time; // to get the time since that moment
	
	    $tokens = array (
	        31536000 => 'year',
	        2592000 => 'month',
	        604800 => 'week',
	        86400 => 'day',
	        3600 => 'hour',
	        60 => 'min',
	        1 => 'second'
	    );
	
	    foreach ($tokens as $unit => $text) {
	        if ($time < $unit) continue;
	        $numberOfUnits = floor($time / $unit);
	        return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
	    }
	
	}
	
	if (! function_exists('array_column')) {
	    function array_column(array $input, $columnKey, $indexKey = null) {
	        $array = array();
	        foreach ($input as $value) {
	            if ( ! isset($value[$columnKey])) {
	                trigger_error("Key \"$columnKey\" does not exist in array");
	                return false;
	            }
	            if (is_null($indexKey)) {
	                $array[] = $value[$columnKey];
	            }
	            else {
	                if ( ! isset($value[$indexKey])) {
	                    trigger_error("Key \"$indexKey\" does not exist in array");
	                    return false;
	                }
	                if ( ! is_scalar($value[$indexKey])) {
	                    trigger_error("Key \"$indexKey\" does not contain scalar value");
	                    return false;
	                }
	                $array[$value[$indexKey]] = $value[$columnKey];
	            }
	        }
	        return $array;
	    }
	}
	
?>