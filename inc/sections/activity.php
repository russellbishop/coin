<?php
	
	/*
		get latest records
	*/
	$activities = $database->select('records',
		[
			'[><]types-define' => ['type_id' => 'id']
		],
		
		[
			'records.id',
			'types-define.type',
			'records.amount',
			'records.datetime'
		],
		
		[
			'ORDER' => 'datetime DESC',
			'LIMIT' => 5,
		]
	);
	
?>

<section class="section activity container">
	
	<ol class="records">
		
		<?php
			
			$i = 0;
			
			foreach ($activities as $activity) {
				
				$i++;
				
				$time = strtotime($activity["datetime"]);
				
				echo '<li>';
				echo '<a href="#" class="popup-generate"
					data-popup="delete-record-activity"
					data-popup-type="' . $activity["type"] . '"
					data-popup-since="' . timeSince($time) . '"
					data-popup-amount="' . $activity["amount"] . '"
					data-popup-record-id="' . $activity["id"] . '"
					data-root="' . $ROOT . '"
					data-popup-action="' . $ACTIONS . 'records-delete.php">';
				echo '<h2 class="label">' . $activity["type"] . ' <span class="since">' . timeSince($time) . ' ago</span></h2>';
				echo '<div class="right">';
				
					if ( $activity["amount"] < 0 ) {
						echo '<p class="credit credit-small minus">' . round($activity["amount"]*-1) . '</p>';
					}
					
					else {
						echo '<p class="credit credit-small">' . round($activity["amount"]) . '</p>';
					}
				
				echo '</div>';
				echo '</a>';
				
				echo '</li>';
				
			}
			
		?>
	
	</ol>
	
</section>