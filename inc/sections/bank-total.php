<?php
	
	if ( $account["salary"] == 1 ) {

		$todaysdate = date('Y-m-d H:i:s');
		
		$todaysmonth = date('m');
		
		$todaysday = date('d');
		
		$thismonthspaydate = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m') . '-' . $account["paydate"] . '00:00:00');
		
		
		
		if ((new DateTime($todaysdate)) < $thismonthspaydate) {
			
			// today is before paydate;
			
			// if this is a weekend, modify to the previous friday
			if (in_array($thismonthspaydate->format('N'), array(6, 7))) {
				$thismonthspaydate->modify('last weekday');
			}
			
		}
				
		else {
			
			// today is after paydate;
			
			$thismonthspaydate->modify('+1 month');
			
			// if this is a weekend, modify to the previous friday
			if (in_array($thismonthspaydate->format('N'), array(6, 7))) {
				$thismonthspaydate->modify('last weekday');
			}	
			
		}
		
		$todaysdatezerod = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d') . ' 00:00:00');
		
		$interval = $todaysdatezerod->diff($thismonthspaydate);
	
	}
	
?>

<figure class="artwork-credit">

	<img src="<?=$IMG;?>foreground.min.png" alt="Foreground Graphic" class="foreground" />
	<img src="<?=$IMG;?>background.min.gif" alt="Background Graphic" class="background" />

	<figure class="character">
		<figure class="sprite idle"></figure>
	</figure>

	<div class="total-until">

		<?php // Sum all 'records' and round ?>
		<p class="credit credit-large"><?php echo round($database->sum('records', "amount"));?></p>
		
		<?php if ( $account["salary"] == 1 ) : ?><p class="days-until"><?php echo $interval->format('%a '); ?>days until payday</p><?php endif; ?>
	
	</div>
	
	<div class="animation-explosion">
		
		<?php echo str_repeat('<span class="particle"></span>', 40); ?>
		
	</div>
	
</figure>