<?php
ob_start();
include('config/config.php');
?>

<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="theme-color" content="#010512">

<title>Coin: <?=strtoupper($account["username"]);?></title>

<link rel="stylesheet" href="<?=$ROOT;?>css/screen.css" media="all" />

<link rel="icon" sizes="192x192" href="<?=$ROOT;?>favicons/favicon-192.png">
<link rel="icon" sizes="128x128" href="<?=$ROOT;?>favicons/favicon-128.png">

<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?=$ROOT;?>favicons/apple-icon-57x57.png" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=$ROOT;?>favicons/apple-icon-72x72.png" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=$ROOT;?>favicons/apple-icon-114x114.png" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=$ROOT;?>favicons/apple-icon-144x144.png" />