</head>

<body class="view-<?=$page;?>">
	
<figure class="page-loading loading">

	<div class="loading-art">
		<div class="coin coin1"><?php include($SERVERROOT. 'images/coin-large.svg.php'); ?></div>
		<div class="coin coin2"><?php include($SERVERROOT. 'images/coin-large.svg.php'); ?></div>
		<div class="coin coin3"><?php include($SERVERROOT. 'images/coin-large.svg.php'); ?></div>
		<div class="coin coin4"><?php include($SERVERROOT. 'images/coin-large.svg.php'); ?></div>
		<div class="coin coin5"><?php include($SERVERROOT. 'images/coin-large.svg.php'); ?></div>
	</div>
	
</figure>
	
<header class="header-user">
	<div class="container">
		
		<ul class="menu">
			<li><span class="name popup-launch" data-popup="menu"><figure class="icon icon-menu"></figure><?php if($page=='home'){echo $account["username"];} else {echo ucfirst($page);}?></span>
				<ul class="popup popup-menu">
					<span class="popup-close popup-close-x">X</span>
					
					<li class="data-label">Menu</li>
					
					<li class="dashboard<?php if($page=='home'){echo" active";}?>"><a href="<?=$ROOT;?>">Dashboard</a></li>
					<li class="records<?php if($page=='records'){echo" active";}?>"><a href="<?=$ROOT;?>records.php">Records</a></li>
					<li class="monthlies<?php if($page=='monthlies'){echo" active";}?>"><a href="<?=$ROOT;?>monthlies.php">Monthlies</a></li>
					<li class="rewards<?php if($page=='rewards'){echo" active";}?>"><a href="<?=$ROOT;?>rewards.php">Rewards</a></li>
					<li class="budgets<?php if($page=='budgets'){echo" active";}?>"><a href="<?=$ROOT;?>budgets.php">Budgets</a></li>
					<li class="account<?php if($page=='account'){echo" active";}?>"><a href="<?=$ROOT;?>account.php">Account</a></li>
				</ul>
			</li>
		</ul>
		
		<div class="right">
			<?php
				$rewardtotal = $database->sum("rewards", "amount");
				echo '<p class="credit credit-small credit-reward"><a href="rewards.php">' . $rewardtotal . '</a></p>';
			?>
		</div>
	
	</div>
</header>

<article class="body">