<?php $page = 'monthlies'; ?>

<?php include("inc/header-top.php"); ?>

<?php include($INC."header-bottom.php"); ?>

<?php include($DATA . 'monthlies.php'); // $monthlies
	
// if there aren't any monthlies yet
if (empty($monthlies))  {
	
?>

<section class="content">
	
	<h1>Monthlies&hellip;</h1>
	<h2>&hellip;now what are they?</h2>
		
	<hr />
	
	<p>Monthlies are your monthly costs, like your <b>Rent</b> and your <b>Phone</b> contract.</p>
	
	<p>Once you&rsquo;ve added them here, you can pay all of your monthlies in one tap.</p>
	
	<div class="actions inset">
		<button class="popup-launch submit submit-add col" data-popup="add-monthly">New Monthly</button>
	</div>

</section>

<?php
		
} else {
	
?>

<section class="section monthly container">
	
	<header class="data-label">
		
		<h1 class="label">Monthlies</h1>
		
		<div class="right">
				
			<?php
			
			$monthlycosts = $database->sum('monthly', 'amount');
			
			?>
			
			<p class="credit credit-small minus"><?php echo round($monthlycosts); ?></p>
		
		</div>
		
	</header>
	
	<div id="live-monthly-records">
		<ol class="records">
			
			<?php
				
			foreach ($monthlies as $monthly) {
				
				$typesum = $database->sum('monthly', 'amount', [
					'AND' => [
						'type_id' => $monthly
					]
				]);
				
				if ( $typesum != 0 ) {
					
					echo '<li>';
					echo '<h2 class="label">' . $monthly["type"] . '</h2>';
					echo '<div class="right">';
					echo '<p class="credit credit-small minus">' . round($typesum) . '</p>';
					echo '</div>';
					echo '</li>';
				
				}
		
			}
			
		
			?>
			
		</ol>
	</div>
	
	
	<div class="actions inset">
		<button class="popup-launch submit submit-add col" data-popup="add-monthly">New Monthly</button>
		<button class="popup-launch submit col" data-popup="pay-monthlies">Pay This Month</button>
	</div>
	
</section>

<?php 
	
} // end else

?>

<div class="popup popup-add-monthly <?php /* popup-open */ ?>">
		
	<span class="popup-close popup-close-x">X</span>
	
	<form action="<?=$ACTIONS?>monthly-new.php" method="post" class="form-new-monthly popup-body popup-base">
		
		<fieldset>
			<?php /* <label for="form-monthly-type">Choose a monthly payment:</label> */ ?>
		    <select name="payment_type" class="type" id="form-monthly-type" data-placeholder="Choose&hellip;" style="width: 100%;">
			    
			    <?php /* this is for the placeholder text */ ?>
			    <option></option>
			    
			    <?php
				    
				/* get every type-define */
				$typeswithids = $database->select("types-define", ["type", "id"]);
				
				foreach ($typeswithids as $typewithid) {
					echo '<option value="' . $typewithid["id"] . '">' . $typewithid["type"] . '</option>';
				}
				
				?>
				
		    </select>
		</fieldset>

		<fieldset>
		<label for="form-monthly-amount">Amount</label><input type="number" name="amount" id="form-monthly-amount" placeholder="0" step="0.01" min="0" >
		</fieldset>
		
		<button type="submit" class="submit submit-add">Add New monthly</button>
		
	<div class="loading-art"><img src="<?=$IMG;?>coin-large.svg" /></div>
	
	</form>
</div>

<div class="popup popup-pay-monthlies">
	
	<span class="popup-close popup-close-x">X</span>
	
	<?php include($INC.'forms/form-monthly-add.php'); ?>
</div>

<?php include($INC."footer-top.php"); ?>

<script src="<?=$JS;?>monthly-new.js"></script>

<?php include($INC."footer-bottom.php"); ?>