<svg width="21px" height="33px" viewBox="0 0 21 33" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
    <!-- Generator: Sketch 3.3.1 (12005) - http://www.bohemiancoding.com/sketch -->
    <title>Rectangle 3 + C</title>
    <desc>Created with Sketch.</desc>
    <defs>
        <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-1">
            <feOffset dx="2" dy="2" in="SourceAlpha" result="shadowOffsetInner1"></feOffset>
            <feGaussianBlur stdDeviation="0" in="shadowOffsetInner1" result="shadowBlurInner1"></feGaussianBlur>
            <feComposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></feComposite>
            <feColorMatrix values="0 0 0 0 1   0 0 0 0 0.960784314   0 0 0 0 0.776470588  0 0 0 0.7 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></feColorMatrix>
            <feMerge>
                <feMergeNode in="SourceGraphic"></feMergeNode>
                <feMergeNode in="shadowMatrixInner1"></feMergeNode>
            </feMerge>
        </filter>
        <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-2">
            <feOffset dx="-1" dy="1" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
            <feGaussianBlur stdDeviation="0" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
            <feColorMatrix values="0 0 0 0 1   0 0 0 0 0.959463059   0 0 0 0 0.777797506  0 0 0 0.7 0" in="shadowBlurOuter1" type="matrix" result="shadowMatrixOuter1"></feColorMatrix>
            <feMerge>
                <feMergeNode in="shadowMatrixOuter1"></feMergeNode>
                <feMergeNode in="SourceGraphic"></feMergeNode>
            </feMerge>
        </filter>
    </defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
        <g id="Rectangle-3-+-C" sketch:type="MSLayerGroup">
            <path d="M15.6237929,32.2032965 L13.0198274,32.2032965 L10.4158619,32.2032965 L7.81189645,32.2032965 L5.20793096,32.2032965 L2.60396548,32.2032965 L2.60396548,29.5196885 L0,29.5196885 L0,26.8360804 L0,24.1524724 L0,21.4688643 L0,18.7852563 L0,16.1016483 L0,13.4180402 L0,10.7344322 L0,8.05082413 L0,5.36721609 L0,2.68360804 L2.60396548,2.68360804 L2.60396548,0 L5.20793096,0 L7.81189645,0 L10.4158619,0 L13.0198274,0 L15.6237929,0 L18.2277584,0 L18.2277584,2.68360804 L20.8317239,2.68360804 L20.8317239,5.36721609 L20.8317239,8.05082413 L20.8317239,10.7344322 L20.8317239,13.4180402 L20.8317239,16.1016483 L20.8317239,18.7852563 L20.8317239,21.4688643 L20.8317239,24.1524724 L20.8317239,26.8360804 L20.8317239,29.5196885 L18.2277584,29.5196885 L18.2277584,32.2032965 L15.6237929,32.2032965 Z" id="Rectangle-3" fill="#F8DD64" filter="url(#filter-1)" sketch:type="MSShapeGroup"></path>
            <path d="M13.6370968,24.8025266 L12.1975806,24.8025266 L10.7580645,24.8025266 L9.31854839,24.8025266 L7.87903226,24.8025266 L6.43951613,24.8025266 L6.43951613,23.3189827 L5,23.3189827 L5,21.8354388 L5,20.3518949 L5,18.8683511 L5,17.3848072 L5,15.9012633 L5,14.4177194 L5,12.9341755 L5,11.4506316 L5,9.96708777 L5,8.48354388 L6.43951613,8.48354388 L6.43951613,7 L7.87903226,7 L9.31854839,7 L10.7580645,7 L12.1975806,7 L13.6370968,7 L15.0766129,7 L15.0766129,8.48354388 L16.516129,8.48354388 L16.516129,9.96708777 L16.516129,11.4506316 L16.516129,12.9341755 L15.0766129,12.9341755 L13.6370968,12.9341755 L13.6370968,11.4506316 L13.6370968,9.96708777 L12.1975806,9.96708777 L10.7580645,9.96708777 L9.31854839,9.96708777 L7.87903226,9.96708777 L7.87903226,11.4506316 L7.87903226,12.9341755 L7.87903226,14.4177194 L7.87903226,15.9012633 L7.87903226,17.3848072 L7.87903226,18.8683511 L7.87903226,20.3518949 L7.87903226,21.8354388 L9.31854839,21.8354388 L10.7580645,21.8354388 L12.1975806,21.8354388 L13.6370968,21.8354388 L13.6370968,20.3518949 L13.6370968,18.8683511 L15.0766129,18.8683511 L16.516129,18.8683511 L16.516129,20.3518949 L16.516129,21.8354388 L16.516129,23.3189827 L15.0766129,23.3189827 L15.0766129,24.8025266 L13.6370968,24.8025266 Z" id="C" fill="#101418" filter="url(#filter-2)" sketch:type="MSShapeGroup"></path>
        </g>
    </g>
</svg>