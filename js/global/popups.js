$(function() {
	
	$('.popup-launch').click(function(e) {
		
		var type = $(this).data('popup');
		$('.popup-' + type).addClass('popup-open');
		
		e.stopPropagation();
		
	});

	
	/* If an event gets to the body
	$("body").click(function(){
		$(".popup").removeClass("popup-open");
		$("body").removeClass("animation-pause");
	});
	
	// Prevent events from getting past .popup
	$(".popup-body, .popup-menu").click(function(e){
		e.stopPropagation();
	});
	*/
	
	
	// Close any popup by removing .popup-open
	$(document).on('click', '.popup-close', function() {
		$(this).parents('.popup').removeClass('popup-open');
		$('body').removeClass('animation-pause');
	});
	
	// Delete any generated popup
	$(document).on('click', '.popup-delete', function() {
		$(this).parents('.popup').remove();
		$('body').removeClass('animation-pause');
	});
	
	
	
	$(document).on('click', '.popup-generate', function (e) {
		
		var popup_kind = $(this).data('popup');
		
		// delete record from activity
		if (popup_kind == 'delete-record-activity') {
			
			var root = $(this).data('root');
			var popup_type = $(this).data('popup-type');
			var popup_since = $(this).data('popup-since');
			var popup_amount = $(this).data('popup-amount');
			var popup_record_id = $(this).data('popup-record-id');
			var popup_action = $(this).data('popup-action');
			
			var popup_html = $('<div class="popup popup-delete-activity popup-open">\
			<span class="popup-delete popup-close-x">X</span>\
			<form class="form form-delete popup-body popup-base" action="' + popup_action + '" method="post">\
			<h4 class="type">' + popup_type + '</h4>\
			<p class="since">' + popup_since + ' ago</p>\
			<p class="amount">\
			<span class="credit credit-small' + ((popup_amount > 0 )? "" : " minus") + '">' + ((popup_amount > 0 ) ? popup_amount : Math.abs(popup_amount)) + '</span>\
			</p>\
			<input type="submit" name="reward" value="Delete?" class="submit submit-delete">\
			<input type="number" name="id" value="' + popup_record_id + '" class="hidden">\
			<div class="loading-art"><img src="' + root + 'images/coin-large.svg" /></div>\
			</form>\
			</div>');
			
			popup_html.appendTo("body");
		}
		
		// delete record from records page
		if (popup_kind == 'delete-record') {
			
			var root = $(this).data('root');
			var popup_type = $(this).data('popup-type');
			var popup_amount = $(this).data('popup-amount');
			var popup_when = $(this).data('popup-when');
			var popup_record_id = $(this).data('popup-record-id');
			var popup_action = $(this).data('popup-action');
			
			var popup_html = $('<div class="popup popup-delete-record popup-open">\
			<span class="popup-delete popup-close-x">X</span>\
			<form class="form form-delete popup-body popup-base" action="' + popup_action + '" method="post">\
			<h4 class="type">' + popup_type + '</h4>\
			<p class="when">' + popup_when + '</p>\
			<p class="amount">\
			<span class="credit credit-small' + ((popup_amount > 0 )? "" : " minus") + '">' + ((popup_amount > 0 ) ? popup_amount : Math.abs(popup_amount)) + '</span>\
			</p>\
			<input type="submit" name="reward" value="Delete?" class="submit submit-delete">\
			<input type="number" name="id" value="' + popup_record_id + '" class="hidden">\
			<div class="loading-art"><img src="' + root + 'images/coin-large.svg" /></div>\
			</form>\
			</div>');
			
			popup_html.appendTo("body");
		}
		
		e.preventDefault();
		e.stopPropagation();
		
	});
	
});