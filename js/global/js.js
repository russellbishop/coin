$(window).load(function() {
	
	$('body').addClass('images-ready');
	
});


$(document).ready(function(){

	// For iOS Webapps to stay in the same window
	$("a").on("click", function(e){
	
		var new_location = $(this).attr("href");
		
		if (new_location != undefined && new_location.substr(0, 1) != "#" && new_location!='' && $(this).attr("data-method") == undefined)
		{
			e.preventDefault();
			window.location = new_location;
		}
	});
	
	/*$('.menu li > a').click(function() {
		$(this).addClass('clicked');
	});*/
	
	// Remove error classes everywhere
	$("input, textarea").bind("change paste keyup", function() {
		$(this).removeClass('error');
	});
	
	$('select').change(function() {
		$('.select2').removeClass('error');
	});
	  
});