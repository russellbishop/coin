$(function(){
	
	$('select.type').select2({
		tags: true
	});
	
	$('input[name="amount"]').bind('change paste keyup', function() {
		$(this).removeClass('error');
	});
	
	$('.type').change(function() {
		$('.select2').removeClass('error');
	});
	
	
	

    function processMonthlyNew(e){
	    
	    e.preventDefault();
	    
	    var form_monthly_new = $('.form-new-monthly');

	    /* throw errors */
        if ( !$('input[name="amount"]').val() || !$('.type').val() ) {
			
			if ( !$('input[name="amount"]').val() ) {
				$('input[name="amount"]').addClass('error');
			}
			
			if ( !$('.type').val() ) {
				$('.select2').addClass('error');
			}
		}
		
		/* post */
		else {
	    
	        $(form_monthly_new).addClass('loading');
	        
	        setTimeout(function () {
	        
	            $.ajax({
	                url: form_monthly_new.attr('action'),
	                dataType: 'text',
	                type: 'post',
	                contentType: 'application/x-www-form-urlencoded',
	                data: $(form_monthly_new).serialize(),
	                
	                success: function( data, textStatus, jQxhr ){
		                
		                var newdata = $.parseJSON(data);
		                
		                console.log(newdata);
						
						$(form_monthly_new).removeClass('loading');
												
						$('#live-monthly-records').load("monthlies.php #live-monthly-records .records");
						
						$('.popup-open').removeClass('popup-open');
											
	                }
	            });
	            
	        }, 500);
        
        }
        
    }

    $(document).on('submit', '.form-new-monthly', processMonthlyNew);
    
});