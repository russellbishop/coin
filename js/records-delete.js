$(function(){

    function processRecordDelete(e){
	    
	    var popup = $('.popup-delete-record');
	    var form_delete = $('.form-delete');
	    
	    e.preventDefault();
        
        $(form_delete).addClass('loading');
        
        setTimeout(function () {
        
            $.ajax({
                url: form_delete.attr('action'),
                dataType: 'text',
                type: 'post',
                contentType: 'application/x-www-form-urlencoded',
                data: $(form_delete).serialize(),
                
                success: function( data, textStatus, jQxhr ){
	                
	                var newdata = $.parseJSON(data);
	                
					$("li[data-id='" + newdata['id'] + "']").fadeOut(500, function(){ 
					    $(this).remove();
					});
					
					$(popup).remove();
											
                }
            });
            
        }, 1000);
        
    }

    $(document).on('submit', '.form-delete', processRecordDelete);
    
});