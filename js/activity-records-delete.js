$(function(){

    function processRecordDelete(e){
	    
	    var popup = $('.popup-delete-activity');
	    var form_delete = $('.form-delete');
	    
	    e.preventDefault();
        
        $(form_delete).addClass('loading');
        
        setTimeout(function () {
        
            $.ajax({
                url: form_delete.attr('action'),
                dataType: 'text',
                type: 'post',
                contentType: 'application/x-www-form-urlencoded',
                data: $(form_delete).serialize(),
                
                success: function( data, textStatus, jQxhr ){
	                
	                var newdata = $.parseJSON(data);
	                
	                $(popup).remove();
					$("body").removeClass("animation-pause");
	                
	                $('.total-until .credit')
	                	.addClass('animate-zoomAway')
	                	.delay(300)
	                	.queue(function(){
		                	
							$(this).html(newdata['newtotal'])
							.removeClass('animate-zoomAway')
							.addClass('animate-zoomTowards')
							.dequeue();
							
							$('body').addClass('status-counted').dequeue();
						})
						.delay(1500)
						.queue(function(){
							$(this).removeClass('animate-zoomTowards')
							.dequeue();
							
							$('body').removeClass('status-counted').dequeue();
						});
					
					$(".activity").load("index.php .activity .records");
										
                }
            });
            
        }, 1000);
        
    }

    $(document).on('submit', '.form-delete', processRecordDelete);
    
});