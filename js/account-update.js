$(function(){
	
	$('input').bind('change paste keyup', function() {
		$(this).removeClass('error');
	});
	
	$('select').change(function() {
		$(this).removeClass('error');
	});
	
	if ( $('#account-salaried').val() == 0 ) {
		$('.payday-toggle').hide();
	}
	
	$('#account-salaried').on('change', function() {
		
		if ( $(this).val() == 0 )
			{
				$('.payday-toggle').hide();
			}
			
		else {
				$('.payday-toggle').show();
			}

	});
	
	
    function processAccountUpdate(e){
	    
	    e.preventDefault();
	    
	    var form_account = $('.form-account');

        $(form_account).addClass('loading');
        
        setTimeout(function () {
        
            $.ajax({
                url: form_account.attr('action'),
                dataType: 'text',
                type: 'post',
                contentType: 'application/x-www-form-urlencoded',
                data: $(form_account).serialize(),
                
                success: function( data, textStatus, jQxhr ){
	                
	                $(form_account).removeClass('loading');
	                
	                $(".submit").html('Updated!').removeClass('submit-add').addClass('submit-earn');
	                
	               /*  var newdata = $.parseJSON(data);
	                
	                console.log(newdata);
					
					
											
					
					
					$('.popup-open').removeClass('popup-open');
					
					*/
										
                }
            });
            
        }, 500);
        
    }

    $(document).on('submit', '.form-account', processAccountUpdate);
    
});