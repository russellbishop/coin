$(function(){
	 
	$('select.type').select2({
		tags: true
	});
	
	var form = $('.form-send');
	 
        function processForm(e){
	        
	        /* throw errors */
	        if ( !$(form).find('.amount').val() || !$(form).find('.type').val() ) {
				
				if ( !$(form).find('.amount').val() ) {
					$('.amount').addClass('error');
				}
				
				if ( !$(form).find('.type').val() ) {
					$('.select2').addClass('error');
				}
			}
			
			/* post it */
			else {
				
				var sendspendearn = $(".submit-form[clicked=true]").val()
	        
		        $(form).addClass('loading');
		        
		            $.ajax({
		                url: form.attr('action'),
		                dataType: 'text',
		                type: 'post',
		                contentType: 'application/x-www-form-urlencoded',
		                data: $(form).serialize()+'&'+$.param({ 'send-spend-earn': sendspendearn }),
		                
		                success: function( data, textStatus, jQxhr ){
			                
			                var newdata = $.parseJSON(data);
			                
			                $('.total-until .credit')
			                	.addClass('animate-zoomAway')
			                	.delay(300)
			                	.queue(function(){
				                	
									$(this).html(newdata['newtotal'])
									.removeClass('animate-zoomAway')
									.addClass('animate-zoomTowards')
									.dequeue();
									
									$('body').addClass('status-counted').dequeue();
								})
								.delay(1500)
								.queue(function(){
									$(this).removeClass('animate-zoomTowards')
									.dequeue();
									
									$('body').removeClass('status-counted').dequeue();
								});
							
							$(form).removeClass('loading');
							
							if (newdata['isbudget'] == 'yes') {
								
								var template = _.template($('#budget-remaining').html());
            
					            $('body').append(template({
						            budgettype: newdata['budgettype'],
					                budgetremaining: newdata['budgetremaining'],
					            }));
								
							} else if (newdata['spentthismonth'] < 0) {
								
								/* TOO ANNOYING?
									
								var template = _.template($('#spent-this-month').html());
            
					            $('body').append(template({
						            spentthismonth: newdata['spentthismonth'],
					                typename: newdata['typename'],
					                typeid: newdata['typeid'],
					            }));
					            
					            */
								
							}
							
							// reset fields
							$(form).find('input').val('');
							$('.type').select2('val', '');
							
							// load in new records
							$(".activity").load("index.php .activity .records");
												
		                },
		                
		                error: function( jqXhr, textStatus, errorThrown ){
		                    console.log( errorThrown );
		                }
		            });
		            
		    }

            e.preventDefault();
        }

        $(form).submit( processForm );
        
        $('.submit-form').click(function() {
		    $('.submit-form', $(this).parents('form')).removeAttr('clicked');
		    $(this).attr('clicked', 'true');
		});
        
});