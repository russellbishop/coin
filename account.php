<?php $page = 'account'; ?>

<?php include("inc/header-top.php"); ?>

<?php include($INC."header-bottom.php"); ?>

<section class="container">
	
	<h1 class="page-title">Account</h1>
	
	<form method="POST" action="<?=$ACTIONS?>account-update.php" class="form form-account">
	
		<fieldset>
			<label for="account-username">Name:</label>
			<input type="text" name="account-username" id="account-username" value="<?=$account["username"];?>" />
		</fieldset>
		
		<fieldset>
			<label for="account-salaried">Are your salaried?:</label>
			<select name="account-salaried" id="account-salaried">
				<option value="1" <?php if ($account["salary"] == "1") { echo "selected"; } ?>>Yes, on a salary.</option>
				<option value="0" <?php if ($account["salary"] == "0") { echo "selected"; } ?>>Nope, not me!</option>
			</select>
		</fieldset>		
		
		<fieldset class="payday-toggle">
			<label for="account-paydate">Monthly Payday:</label>
			
			<select name="account-paydate" id="account-paydate" class="paydate">
				<option value="18" <?php if ( $account["paydate"] == 18 ) { echo 'selected="selected"'; } ?>>18th</option>
				<option value="19" <?php if ( $account["paydate"] == 19 ) { echo 'selected="selected"'; } ?>>19th</option>
				<option value="20" <?php if ( $account["paydate"] == 20 ) { echo 'selected="selected"'; } ?>>20th</option>
				<option value="21" <?php if ( $account["paydate"] == 21 ) { echo 'selected="selected"'; } ?>>21st</option>
				<option value="22" <?php if ( $account["paydate"] == 22 ) { echo 'selected="selected"'; } ?>>22nd</option>
				<option value="23" <?php if ( $account["paydate"] == 23 ) { echo 'selected="selected"'; } ?>>23rd</option>
				<option value="24" <?php if ( $account["paydate"] == 24 ) { echo 'selected="selected"'; } ?>>24th</option>
				<option value="25" <?php if ( $account["paydate"] == 25 ) { echo 'selected="selected"'; } ?>>25th</option>
				<option value="26" <?php if ( $account["paydate"] == 26 ) { echo 'selected="selected"'; } ?>>26th</option>
				<option value="27" <?php if ( $account["paydate"] == 27 ) { echo 'selected="selected"'; } ?>>27th</option>
				<option value="28" <?php if ( $account["paydate"] == 28 ) { echo 'selected="selected"'; } ?>>28th</option>
				<option value="29" <?php if ( $account["paydate"] == 29 ) { echo 'selected="selected"'; } ?>>29th</option>
				<option value="30" <?php if ( $account["paydate"] == 30 ) { echo 'selected="selected"'; } ?>>30th</option>
				<option value="31" <?php if ( $account["paydate"] == 31 ) { echo 'selected="selected"'; } ?>>31st</option>
			</select>
		</fieldset>
		
		<button type="submit" class="submit submit-add">Update my Account</button>
		
		<div class="loading-art"><img src="<?=$IMG;?>coin-large.svg" /></div>
		
	</form>
	
</section>


<?php include($INC."footer-top.php"); ?>

<script src="<?=$JS;?>account-update.js"></script>

<?php include($INC."footer-bottom.php"); ?>