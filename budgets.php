<?php $page = 'budgets'; ?>

<?php include("inc/header-top.php"); ?>

<?php include($INC."header-bottom.php"); ?>

<?php
	
	// get every budget and join with types-define
	$budgets = $database->select("budgets", 
		[
			'[><]types-define' => ['type_id' => 'id']
		],
		
		[
			'budgets.amount',
			'types-define.id',
			'types-define.type'
		]
	);

?>

<section class="container">
	
	<h1 class="page-title">Budgets</h1>
	
</section>

<?php include($INC.'forms/form-budgets.php'); ?>

<section class="container">

	<h1 class="page-title">This month</h1>

</section>

<ol class="budgets records container">
			
	<?php
		
	foreach ($budgets as $budget) {
			
			$budgetsum = $database->sum('records', 'amount', [
				'AND' => [
					'datetime[<>]' => $thisMonthsDates,
					'type_id' => $budget["id"]
				]
			]);
				
			echo '<li>';
			
			echo '<div class="right">';
				echo '<p class="credit-fraction">';
				
				if ($budgetsum < 0 ) {
					echo '<span class="credit credit-small' . ((($budgetsum*-1) > $budget["amount"])?' minus':"") . '">' . ($budgetsum*-1) . '</span>';
					echo '<span class="credit-slash">/</span>';
				}
				
				echo '<span class="credit credit-small neutral">' . floatval($budget["amount"]) . '</span>';
				echo '</p>';
			echo '</div>';
			
			echo '<h2 class="title">' . $budget['type'] . '</h2>';
			
			echo '</li>';
			
		}

	?>
		
</ol>

<?php include($INC.'sections/send-sound.php'); ?>

<?php include($INC.'footer-top.php'); ?>

<script>

$(function () {
	
	$('select.type').select2({
		language: {
            noResults: function(term) {
                return "No matching types!";
            }
        }
	});
	
	$("form .amount").bind("change paste keyup", function() {
		$(this).removeClass('error');
	});
	
	$('form .type').change(function() {
		$('.select2').removeClass('error');
	});
	
});

</script>

<?php include($INC."footer-bottom.php"); ?>