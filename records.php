<?php $page = 'records'; ?>

<?php include("inc/header-top.php"); ?>

<?php include($INC."header-bottom.php"); ?>

<?php

/* create empty array
$payDates = array();

// first and last record's months
$firstrecord = $database->min("records", "datetime");
$lastrecord = $database->max("records", "datetime");

// convert to 201501 to 201512
$fm = date("Ym", strtotime($firstrecord));
$lm = date("Ym", strtotime($lastrecord));

// create months
foreach (range($fm, $lm) as $i => $monthNum) {
	
	// Before comma is the format, after is the date:
	$dt = DateTime::createFromFormat('Ym d H:i:s', $monthNum . ' ' . $account["paydate"] . ' 00:00:00');
	$et = clone $dt;
  
	// set $lt to last month
	$et->modify('-1 month');
    
	// if it's a weekend, modify the date to the last weekday
	if (in_array($et->format('N'), array(6, 7))) {
		$et->modify('last weekday');
  	}
  	if (in_array($dt->format('N'), array(6, 7))) {
  		$dt->modify('last weekday');
    }
    
    $dt->modify('-1 second');
    
    $payDates[] = array(
      'name' => $dt->format('F'),
      'start' => $et->format('Y-m-d H:i:s'),
      'end' => $dt->format('Y-m-d H:i:s')
    );
  	
}

*/





// create empty array
$payDates = array();

// first and last record's months
$firstrecord = $database->min("records", "datetime");
$lastrecord = $database->max("records", "datetime");

// convert to 201501 to 201512
$fm = date("Ym", strtotime($firstrecord));
$lm = date("Ym", strtotime($lastrecord));

// create months
foreach (range($fm, $lm) as $i => $yearMonth) {
	
	$monthstart = DateTime::createFromFormat('Ym d H:i:s', $yearMonth . ' 01 00:00:00');
	$monthend = clone $monthstart;
	
	$monthend->modify('+1 month');
	$monthend->modify('-1 second');

    $payDates[] = array(
      'name' => $monthstart->format('F'),
      'start' => $monthstart->format('Y-m-d H:i:s'),
      'end' => $monthend->format('Y-m-d H:i:s')
    );
  	
}



?>

<nav class="months-pager">
	<span class="months-prev">&#8249;</span>
	<div class="months-pagers"></div>
	<span class="months-next">&#8250;</span>
</nav>

<article class="cycle-slideshow months section"
	data-cycle-fx="fade"
	data-cycle-timeout="0"
	data-cycle-slides="> .month-records"
	data-cycle-pager=".months-pagers"
	data-cycle-next=".months-next"
	data-cycle-prev=".months-prev"
	data-cycle-swipe="true"
	data-cycle-auto-height="container"
	data-cycle-swipe="true"
	data-cycle-hide-non-active="true"
    data-cycle-swipe-fx="scrollHorz">
    
    
<?php
    
foreach ( array_reverse($payDates) as $dateRange ) {
	
	// get this month's records
	
	$thismonthsrecords = $database->select('records',
		[
			'[><]types-define' => ['type_id' => 'id']
		],
		
		[
			'records.id',
			'records.type_id',
			'types-define.type',
			'records.amount',
			'records.datetime'
		],
		
		[
			'datetime[<>]' => [$dateRange['start'], $dateRange['end']]
		]
		
	);
	
?>

	<section class="month-records">
	
		<header class="data-label">
			
			<h1 class="label"><?php echo $dateRange["name"]; ?></h1>
			
			<p class="data-range"><?php echo date("M jS", strtotime($dateRange["start"])) . ' - ' . date("M jS", strtotime($dateRange['end'])); ?></p>
			
			<div class="month-sum right">
				
				<?php
					
					// get all this month's amounts
					$amounts = array_column($thismonthsrecords, 'amount');
					
					$positivesum = 0;
					foreach($amounts as $num => $value) {
						if ($value > 0) {
							 $positivesum += $value;
						}
					}
					
					$negativesum = 0;
					foreach($amounts as $num => $value) {
						if ($value < 0) {
							 $negativesum += $value;
						}
					}
					
					// sum this month's amount
					$amountsum = array_sum($amounts);
					
				?>
				
			</div>
			
		</header>
		
		<ol class="sums">
			
			<li>In: <span class="credit credit-small"><?php echo round($positivesum); ?></span></li>
			<li>Out: <span class="credit credit-small minus"><?php echo round($negativesum); ?></span></li>
			<li>Sum: <span class="credit credit-small<?php if ($amountsum < 0) { echo " minus"; } ?>"><?php echo round($amountsum); ?></span></li>
			
		</ol>
		
		<ol class="records">
			
		<?php
			
			
			$thismonthsrecordsgrouped = array();
	
			foreach($thismonthsrecords as $key => $item)
			{
			   $thismonthsrecordsgrouped[$item['type']][$key] = $item;
			}
			
			ksort($thismonthsrecordsgrouped, SORT_ASC);
			
			
			foreach($thismonthsrecordsgrouped as $row => $innerArray) {
				
				$thistypeIDs = array_column($innerArray, 'type_id');
				
				$thistypeID = array_shift($thistypeIDs);
				
				// find a budget
				$budgetamount = $database->get('budgets', ['id', 'amount'], [
					'type_id' => $thistypeID
				]);
				
				// echo $budgetamount["amount"];
				
				$typesum = array_sum(array_column($innerArray, 'amount'));
				
				echo '<li class="group" data-total="' . $typesum . '"><div class="record-toggle">';
				
					echo '<h2 class="label">';
					
						print($row);
						
						if (count($innerArray) > 1 ) {
							echo ' <span class="count">(' . count($innerArray) . ')</span>';
						}
						
					echo '</h2>';
					
					if ($budgetamount["amount"] > 0) {
					
						echo '<div class="right">';
								echo '<p class="credit-fraction">';
									
									if ( $typesum < 0 ) {
										echo '<span class="credit credit-small minus">' . round($typesum*-1) . '</span>';
									} else {
										echo '<span class="credit credit-small">' . round($typesum) . '</span>';
									}
									
									echo '<span class="credit-slash">/</span>';
									echo '<span class="credit credit-small neutral">' . round($budgetamount["amount"]) . '</span>';
								
								echo '</p>';
						echo '</div>';
					
					} else {
						
						echo '<div class="right">';
								echo '<p class="credit-fraction">';
									
									if ( $typesum < 0 ) {
										echo '<span class="credit credit-small minus">' . round($typesum*-1) . '</span>';
									} else {
										echo '<span class="credit credit-small">' . round($typesum) . '</span>';
									}
									
								echo '</p>';
						echo '</div>';
						
					}
					
					echo '</div>'; //end record-toggle

				echo '<ul class="history">';
									
					foreach ($innerArray as $item) {
						
						echo '<li data-id="' . $item["id"] . '">';
						
							echo '<a href="#" class="popup-generate"
								data-popup="delete-record" 
								data-popup-type="' . $row . '"
								data-popup-amount="' . $item["amount"] . '"
								data-popup-record-id="' . $item["id"] . '"
								data-popup-when="' . date("M jS, g:s A", strtotime($item["datetime"])) . '"
								data-root="' . $ROOT . '"
								data-popup-action="' . $ACTIONS . 'records-delete.php">';
							
								echo '<h2 class="label">' . date("M jS, g:s A", strtotime($item["datetime"])) . '</h2>';
								
								echo '<div class="right">';
								
									if ($item["amount"] < 0 ) { 
										echo '<p class="credit credit-small minus">' . round($item["amount"]*-1) . '</p>';
									}
									else {
										echo '<p class="credit credit-small">' . round($item["amount"]) . '</p>';
									}
									
								echo '</div>';
							
							echo '</a>';
							
						echo '</li>';
					}
					
				echo '</ul>';
				
			}
			
		?>
			
		</ol>
	
	</section>

<?php
	
} //foreach
		
?>
	
</article>

<?php include($INC."footer-top.php"); ?>

<script src="<?=$JS;?>cycle2.min.js"></script>
<script src="<?=$JS;?>records-delete.js"></script>

<script>
<?php /*
$(window).scroll(function(){
	var sticky = $('.month-records .data-label'),
	scroll = $(window).scrollTop();

	if (scroll >= 100) sticky.addClass('fixed');
	else sticky.removeClass('fixed');
});
*/ ?>

$(function() {
	
	$('.record-toggle').click(function(){
		
		/* make this open */
		$(this).parent().toggleClass('open');
		// $(this).next('ul').slideToggle();
		
	});
		
	<?php /*
	$('.records').each(function() {
	
		$(this).find('> li').sort(sort_li).appendTo(this);
		
		function sort_li(a, b){
		    return ($(b).data('total')) < ($(a).data('total')) ? 1 : -1;    
		}
	
	});
	*/ ?>
});
</script>

<?php include($INC."footer-bottom.php"); ?>