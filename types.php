<?php
	
	$page = 'types';
	include("inc/header-top.php");
	
?>


<?php

	include($INC."header-bottom.php");

?>

<?php
	
// if there aren't any monthlies yet
if (empty($rewardtypes))  {
	
?>

<section class="content">
	
	<h1>Payment Types&hellip;</h1>
	<h2>&hellip;what are they?</h2>
		
	<hr />
	
	<p>Rewards are goals that you've set for yourself!</p>
	
	<p>Try some rewards like:</p>
	
	<ul>
		<li>Exercise twice a week.</li>
		<li>Read a book</li>
	</ul>
	
	<div class="inset">	
		<button class="popup-launch submit submit-add" data-popup="add-reward">New Reward</button>
	</div>
	
</section>

<?php
		
} else {
	
?>

<section class="rewards container">
	
	<section class="section">
		
	<header class="data-label">
		
		<h1 class="label">Rewards</h1>
		
	</header>
	
	<ol class="rewards-define records">
		
		<?php
				
		foreach ($rewardtypes as $rewardtype) {
				
				echo '<li class="grid4">';
				
				echo '<div class="col">';
				echo '<h4 class="title">' .  $rewardtype["reward-name"] . '</h4>';
				echo '<p class="description">' .  $rewardtype["reward-description"] . '</p>';
				echo '</div>';
				
				echo '<form class="form form-reward container col" action="' . $ACTIONS . 'reward-add.php" method="post">';
				echo '<input type="submit" name="reward" value="+' .  $rewardtype["reward-value"] . '" class="submit" />';
				echo '<input type="number" name="amount" value="' .  $rewardtype["reward-value"] . '" class="hidden" />';
				echo '<input type="text" name="type" value="' .  $rewardtype["id"] . '" class="hidden" />';
				echo '</form>';
				
				echo '</li>';
				
			}
	
		?>
		
	</ol>
	
	</section>
	
	<section class="section">
	
	<div class="inset">	
		<button class="popup-launch submit submit-add" data-popup="add-reward">New Reward</button>
	</div>
	
	</section>
	
</section>

<?php 
	
} // end else

?>



<?php
	
// if there aren't any monthlies yet
if (!empty($rewards))  {
	
?>

<div class="container">

	<header class="data-label">
			
		<h1 class="label">Rewarded</h1>
		
	</header>
	
	<section class="section">
	
	<ol id="rewards-latest" class="rewards-latest records">
			
		<?php
				
		foreach ($rewards as $reward) {
				
				echo '<li>';
				
				echo '<div class="right">';
				echo '<p class="credit credit-small credit-reward">' . floatval($reward["amount"]) . '</p>';
				echo '</div>';
				
				echo '<h2 class="title">' . $reward["reward-name"] . '</h2>';
				echo '<p class="description">' . timeSince(strtotime($reward["datetime"])) . ' ago</p>';
				
				echo '</li>';
				
			}
	
		?>
			
	</ol>
	
	</section>

</div>

<?php
	
	}
	
?>

<div class="popup popup-add-reward">
		
	<span class="popup-close popup-close-x">X</span>
	
	<?php include($INC . 'forms/form-reward-new.php'); ?>
	
</div>

<?php
	include($INC."footer-top.php");
?>

<?php
	include($INC."footer-bottom.php");
?>